class Session:
    def __init__(self, sattelite, station, time_begin, time_end, len_session):
        self.__sattelite = sattelite
        self.__station = station
        self.__time_begin = time_begin
        self.__time_end = time_end
        self.__len_session = len_session

    def __str__(self):
        return "{%s | %s | %s | %s | %s}" % (str(self.__station),
                                                     str(self.__sattelite),
                                                     self.__time_begin,
                                                     self.__time_end,
                                                     self.__len_session)
    def get_station(self):
        return self.__station

    def get_sattelite(self):
        return self.__sattelite

    def get_time_begin(self):
        return self.__time_begin

    def get_time_end(self):
        return self.__time_end

    def get_len_session(self):
        return self.__len_session

    def get_times_list(self):
        return [self.__time_begin, self.__time_end, self.__len_session]
