from Sattelites import KinoSattelite, ZorkiySattelite
import os
from datetime import datetime
import calendar
from tqdm import tqdm



class Parser:
    def __init__(self, path):
        self.__path = path

    @staticmethod
    def read_file_lines(path_file):
        try:
            with open(path_file, 'r', encoding='utf-8') as file:
                return file.readlines()
        except FileNotFoundError:
            exit("Файл %s не найден!" % path_file)

    @staticmethod
    def read_file(path_file):
        try:
            with open(path_file, 'r', encoding='utf-8') as file:
                return file.read()
        except FileNotFoundError:
            exit("Файл %s не найден!" % path_file)

    def parse(self):
        results_all = []
        for f in os.listdir(self.__path):
            results = []
            #print(f)
            text = self.read_file(os.path.join(self.__path, f))
            tables = text.split('\n\n\n')[1:]

            tables = [i.split('\n\n')[0] for i in tables]
            for table in tables:
                session = table.split('\n-')[0]
                station, sattelite = session.split('-To-')
                title = table.split('-\n')[1].split('-')[0].replace('\n', '')
                values = table.split('-\n')[2]
                title = title.split()
                title = [title[0], title[1]+title[2]+title[3], title[4]+title[5]+title[6], title[7]+title[8]]
                values = [value.split() for value in values.split('\n')]
                #print(sattelite)
                #print(title)
                for i in values:
                    begin = datetime.strptime('%s %s %s %s' % (i[1], i[2], i[3], i[4]), '%d %b %Y %H:%M:%S.%f')
                    begin_sec = calendar.timegm(begin.timetuple())
                    begin_microsec = i[4].split('.')[1]
                    begin_unixtime = begin_sec + (int(begin_microsec) / 1000)
                    end = datetime.strptime('%s %s %s %s' % (i[5], i[6], i[7], i[8]), '%d %b %Y %H:%M:%S.%f')
                    end_sec = calendar.timegm(end.timetuple())
                    end_microsec = i[8].split('.')[1]
                    end_uinxtime = end_sec + (int(end_microsec) / 1000)
                    results.append([station,
                                    sattelite,
                                    begin_unixtime,
                                    end_uinxtime,
                                    round(end_uinxtime - begin_unixtime, 3)])

            results_all.append(results)

        '''for i in results_all:
            for j in i:
                print(j)'''
        #print(results_all)
        return results_all
