 СЕРВИС ПЛАНИРОВАНИЯ ПЕРЕДАЧИ ДАННЫХ ОТ КОСМИЧЕСКИХ АППАРАТОВ НА ЗЕМНЫЕ СТАНЦИИ
--

Сервис, который позволит автоматически составлять наиболее эффективное расписание передачи данных для каждого типа космических аппаратов и земных станций с учётом исходных данных

![python](https://img.shields.io/badge/ЛЦТ_2023-green.svg)
![python](https://img.shields.io/badge/team-DevBoys-blue)

## Installation
```
$ git clone https://gitlab.com/lavrik2000/lct-2023_task_12.git
$ cd lct-2023_task_12
$ pip3 install pyfiglet
$ python3 main.py
```
## Usage

_Note: параметры по умолчанию. Не забудьте их поменять при необходимости_

```
python3 main.py -b 01.06.2027/03:00:01 -e 01.06.2027/06:26:41 -z DATA_Files/Facility2Constellation -r DATA_Files/Russia2Constellation2
```

### Help
```python3 main.py --help```

* __Output__

```
 ____  _ _                   _
/ ___|(_) |_ _ __ ___  _ __ (_) ___ ___
\___ \| | __| '__/ _ \| '_ \| |/ __/ __|
 ___) | | |_| | | (_) | | | | | (__\__ \
|____/|_|\__|_|  \___/|_| |_|_|\___|___/


usage: main.py [-h] [-b BEGIN] [-e END] [-z ZRV] [-r RUSSIA]

СЕРВИС ПЛАНИРОВАНИЯ ПЕРЕДАЧИ ДАННЫХ ОТ КОСМИЧЕСКИХ АППАРАТОВ НА ЗЕМНЫЕ СТАНЦИИ
по умолчанию:
- begin = 01.06.2027/03:00:01
- end = 01.06.2027/06:26:41
- zrv = DATA_Files/Facility2Constellation
- russia = DATA_Files/Russia2Constellation2

optional arguments:
  -h, --help            show this help message and exit
  -b BEGIN, --begin BEGIN
                        Начало составления расписания. Например: 01.06.2027/03:00:01
  -e END, --end END     Конец составления расписания. Например: 01.06.2027/06:26:41
  -z ZRV, --zrv ZRV     Каталог с файлами зон радиовидимости
  -r RUSSIA, --russia RUSSIA
                        Каталог с файлами пролета на территорией России
```

### Run
```python3 main.py -b 01.06.2027/03:00:01 -e 01.06.2027/06:26:41 -z DATA_Files/Facility2Constellation -r DATA_Files/Russia2Constellation2```

* __Output__

```
 ____  _ _                   _
/ ___|(_) |_ _ __ ___  _ __ (_) ___ ___
\___ \| | __| '__/ _ \| '_ \| |/ __/ __|
 ___) | | |_| | | (_) | | | | | (__\__ \
|____/|_|\__|_|  \___/|_| |_|_|\___|___/


[i] Период составления расписания: 01.06.2027/03:00:01 : 01.06.2027/06:26:41
[i] Каталог существует: DATA_Files/Facility2Constellation
[i] Каталог существует: DATA_Files/Russia2Constellation2
[i] Каталог существует: shedules
[i] Каталог существует: logs\sattelites
[i] Каталог существует: logs\stations
[i] Обработка файлов из каталога: DATA_Files/Facility2Constellation
[i] Обработка файлов из каталога: DATA_Files/Russia2Constellation2
[i] Thread Anadyr1 запущен
[i] Thread Anadyr2 запущен
[i] Thread CapeTown запущен
[i] Thread Delhi запущен
[i] Thread Irkutsk запущен
[i] Thread Magadan1 запущен
[i] Thread Magadan2 запущен
[i] Thread Moscow запущен
[i] Thread Murmansk1 запущен
[i] Thread Murmansk2 запущен
[i] Thread Norilsk запущен
[i] Thread Novosib запущен
[i] Thread RioGallegos запущен
[i] Thread Sumatra запущен
[i] Thread CapeTown is ended. Time spent: 1.99
[i] Thread RioGallegos is ended. Time spent: 0.8
[i] Thread Sumatra is ended. Time spent: 1.05
[i] Thread Delhi is ended. Time spent: 2.72
[i] Thread Anadyr1 is ended. Time spent: 3.34
[i] Thread Anadyr2 is ended. Time spent: 3.29
[i] Thread Magadan2 is ended. Time spent: 2.89
[i] Thread Moscow is ended. Time spent: 2.95
[i] Thread Irkutsk is ended. Time spent: 3.4
[i] Thread Murmansk2 is ended. Time spent: 2.32
[i] Thread Magadan1 is ended. Time spent: 3.3
[i] Thread Murmansk1 is ended. Time spent: 2.61
[i] Thread Norilsk is ended. Time spent: 2.45
[i] Thread Novosib is ended. Time spent: 2.37
[+] Расписание сохранено: ./shedules/Anadyr1_shedule.txt
[+] Расписание сохранено: ./shedules/Anadyr2_shedule.txt
[+] Расписание сохранено: ./shedules/CapeTown_shedule.txt
[+] Расписание сохранено: ./shedules/Delhi_shedule.txt
[+] Расписание сохранено: ./shedules/Irkutsk_shedule.txt
[+] Расписание сохранено: ./shedules/Magadan1_shedule.txt
[+] Расписание сохранено: ./shedules/Magadan2_shedule.txt
[+] Расписание сохранено: ./shedules/Moscow_shedule.txt
[+] Расписание сохранено: ./shedules/Murmansk1_shedule.txt
[+] Расписание сохранено: ./shedules/Murmansk2_shedule.txt
[+] Расписание сохранено: ./shedules/Norilsk_shedule.txt
[+] Расписание сохранено: ./shedules/Novosib_shedule.txt
[+] Расписание сохранено: ./shedules/RioGallegos_shedule.txt
[+] Расписание сохранено: ./shedules/Sumatra_shedule.txt
[i] Return to Thread Main
[+] Программа успешно завершила свою работу
[i] Time: 26.94
```
