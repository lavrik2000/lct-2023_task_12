#import random

class Sattelite:
    def __init__(self, name, ZU_cur, ZU_max, speed_fill_ZU, speed_sending_data, type):
        self._name = name
        self._ZU_cur = ZU_cur  #в Гбит    #random.randint(0, ZU_max)#
        self._ZU_max = ZU_max  #в Гбит
        self._ZU_full_per = round((self._ZU_cur / self._ZU_max), 2) * 100
        self._speed_fill_ZU = speed_fill_ZU  #Гбит.с
        self._speed_sending_data = speed_sending_data  #Гбит.с
        self._type = type
        self._times_sending = {}
        self._times_filling = {}

        self.__rus_times = []

        self.last_ZU_changed = 0

        self.log_save_to_file("Создан объект\n")

    def __str__(self):
        return "{%s | %s | %s | %s}" % (self._name, self._type, self._ZU_cur, self._ZU_full_per)

    def get_name(self):
        return self._name

    def get_ZU_cur(self):
        return self._ZU_cur

    def get_ZU_max(self):
        return self._ZU_max

    def get_speed_fill_ZU(self):
        return self._speed_fill_ZU

    def get_speed_sending_data(self):
        return self._speed_sending_data

    def get_ZU_full_per(self):
        return self._ZU_full_per

    def add_ZU(self, data, t, station):
        data = round(data, 3)
        if self._ZU_cur + data > self._ZU_max:
            log = "Произведена фотосъемка на %s Gbit (%s -> %s). Станция: %s\n" % (self._ZU_max - self._ZU_cur,
                                                                                   self._ZU_cur,
                                                                                   self._ZU_max,
                                                                                   station)
            self._ZU_cur = self._ZU_max
        else:
            log = "Произведена фотосъемка на %s Gbit (%s -> %s). Станция: %s\n" % (data,
                                                                                   self._ZU_cur,
                                                                                   self._ZU_cur + data,
                                                                                   station)
            self._ZU_cur = round(self._ZU_cur + data, 3)
        self._ZU_full_per = round((self._ZU_cur / self._ZU_max), 2) * 100
        self.last_ZU_changed = t
        self.log_save_to_file(log)

    def sub_ZU(self, data, t, station):
        if data > self._ZU_cur:
            log = "Отправлено %s Gbit (%s -> %s). Станция: %s\n" % (self._ZU_cur, self._ZU_cur, 0, station)
            self._ZU_cur = 0
        else:
            log = "Отправлено %s Gbit (%s -> %s). Станция: %s\n" % (data, self._ZU_cur, round(self._ZU_cur - data, 3), station)
            self._ZU_cur = round(self._ZU_cur - data, 3)
        self._ZU_full_per = round((self._ZU_cur / self._ZU_max), 2) * 100
        self.last_ZU_changed = t
        self.log_save_to_file(log)

    def add_rus_time(self, data_list):
        self.__rus_times.append(data_list)

    def get_rus_time(self):
        return self.__rus_times

    def add_times_sending(self, time):
        self._times_sending[time] = True
        self.log_save_to_file("Для интервала с началом в %s будет производиться отправка данных\n" % time)

    def get_times_sending(self, time):
        return False if self._times_sending.get(time) is None else True

    def add_times_filling(self, time):
        self._times_filling[time] = True
        self.log_save_to_file("Для интервала с началом в %s будет производиться фотосъемка\n" % time)

    def get_times_filling(self, time):
        return False if self._times_filling.get(time) is None else True

    def log_save_to_file(self, log):
        with open("./logs/sattelites/" + self._name + "_log.txt", 'a+', encoding='utf-8') as file:
            file.write(log)

    def get_last_ZU_changed(self):
        return self.last_ZU_changed
