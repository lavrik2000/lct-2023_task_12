from Sattelites.Sattelite import *


class ZorkiySattelite(Sattelite):
    def __init__(self, name):
        super().__init__(name=name, ZU_cur=0, ZU_max=0.5*1024*8, speed_fill_ZU=4, speed_sending_data=0.25, type='Zor')
