class Station:
    def __init__(self, name):
        self.__name = name
        self.__schedule = {}

    def __str__(self):
        return "{%s}" % self.__name

    def __eq__(self, other):
        return self.__name == other

    def get_name(self):
        return self.__name

    def add_schedule(self, sattelite_name, times_list):
        if self.__schedule.get(sattelite_name) is None:
            self.__schedule[sattelite_name] = [times_list]
        else:
            self.__schedule[sattelite_name].append(times_list)

    def get_schedule(self):
        return self.__schedule

    def push_schedule(self, schedule):
        self.__schedule = schedule
