from threading import Thread
import functools
import time
from printer import *
from printer import *

# процедура сравнения отрезков
def compare_segments(line_1, line_2):
    # согласно алгоритму поиска нам надо сделать следующее:
    # сначала сравнить первые координаты отрезков и найти максимальное значение
    first_pair = max(line_1[0], line_2[0])
    # далее нам надо сравнить вторые координаты отрезков и найти минимальное
    second_pair = min(line_1[1], line_2[1])
    # теперь выполняем сравнение полученных величин
    # если первая пара меньше второй
    if first_pair < second_pair:
        # то наш результат следующий
        return [first_pair, second_pair]
    # если пары равны, то у нас пересечение это просто точка
    elif first_pair == second_pair:
        return [first_pair]
    # а если пара "большего" больше пары "меньшего", то
    else:
        return []

def my_sort_sessions_begin(session_1, session_2):
    if session_1.get_time_begin() > session_2.get_time_begin():
        return 1
    else:
        return -1

def my_sort_sessions_prioritet(session_1, session_2):
    if session_1[0] < session_2[0]:
        return 1
    else:
        return -1

class myThreadIntervals(Thread):
    def __init__(self, lock, station, sessions_list, time_begin, time_end, offset):
        Thread.__init__(self)
        self.lock = lock
        self.station = station
        self.sessions_list = sessions_list
        self.time_begin = time_begin
        self.time_end = time_end
        self.offset = offset

    def run(self):
        c_zu = 3
        c_dt = 1
        c_fill = 4

        try:

            with self.lock:
                #print("[+] Thread %s запущен" % self.getName())
                info("Thread %s запущен" % self.getName())

            t = self.time_begin
            offset = self.offset
            time_start = time.time()

            self.sessions_list.sort(key=functools.cmp_to_key(my_sort_sessions_begin))
            log = ''
            log_count = 0
            while t < self.time_end:
                log_cur = ''
                prioritet_cur_interval_list = []
                filling_dict = {}

                for session in self.sessions_list:
                    sub_ZU_sek = []
                    if session.get_time_end() < t:
                        continue
                    if session.get_time_begin() > (t + offset):
                        break
                    res_compare = compare_segments([session.get_time_begin(), session.get_time_end()], [t, t + offset])
                    #if res_compare:
                    dt = None
                    if len(res_compare) == 2:
                        sub_ZU_sek = [res_compare[0], res_compare[1]]
                        #log += "Сессия в ЗРВ: %s\n" % str(session)
                        if session.get_time_end() > (t + offset):
                            dt = round(session.get_time_end() - (t + offset), 3)
                        else:
                            dt = 0
                    else:
                        continue

                    fill = 0
                    for time_rus in session.get_sattelite().get_rus_time():
                        if time_rus[1] < t:
                            continue
                        if time_rus[0] > (t + offset):
                            break
                        res_compare = compare_segments([time_rus[0], time_rus[1]], [t, t + offset])
                        if len(res_compare) == 2:
                            log_cur += "Над территорией России: %s\t%s. Для данного интервала: %s\n" % (session.get_sattelite().get_name(), time_rus, [res_compare[0], res_compare[1], res_compare[1] - res_compare[0]])
                            fill = res_compare[1] - res_compare[0]
                            filling_dict[session.get_sattelite().get_name()] = [session.get_sattelite(), fill]
                        break

                    zu = session.get_sattelite().get_ZU_full_per()

                    if zu != 0:
                        prioritet = (c_zu * zu) - (c_dt * dt) - (c_fill * fill)
                        prioritet_cur_interval_list.append([prioritet, sub_ZU_sek, session])

                if prioritet_cur_interval_list:
                    prioritet_cur_interval_list.sort(key=functools.cmp_to_key(my_sort_sessions_prioritet))

                sattelite_name_sending_data = ''
                for session_prioritet in prioritet_cur_interval_list:
                    if session_prioritet[0] == 0:
                        break
                    with self.lock:
                        if not session_prioritet[2].get_sattelite().get_times_sending(t) and\
                                session_prioritet[2].get_sattelite().get_last_ZU_changed() < t:
                            sattelite_name_sending_data = session_prioritet[2].get_sattelite().get_name()
                            log_cur += "Наибольший приоритет имеет(а также свободен): %s\n" % str(session_prioritet[2])
                            session_prioritet[2].get_sattelite().add_times_sending(t)
                            send_zu = round(session_prioritet[1][1] - session_prioritet[1][0], 3) * session_prioritet[2].get_sattelite().get_speed_sending_data()
                            session_prioritet[2].get_sattelite().sub_ZU(send_zu, t, self.getName())
                            log_cur += "%s ОТПРАВИЛ %s Gbit (%s s * %s Gbit/s)\n" % (session_prioritet[2].get_sattelite().get_name(),
                                                                                                     send_zu,
                                                                                                     round(session_prioritet[1][1] - session_prioritet[1][0], 3),
                                                                                                     session_prioritet[2].get_sattelite().get_speed_sending_data())
                            session_prioritet[2].get_station().add_schedule(session_prioritet[2].get_sattelite().get_name(),
                                                                            session_prioritet[1])

                            # print(session_prioritet[1].get_station().get_schedule())
                            break

                for key, val in filling_dict.items():
                    if key != sattelite_name_sending_data:
                        if not val[0].get_times_filling(t) and val[0].get_last_ZU_changed() < t:
                            val[0].add_times_filling(t)
                            data_filling = round(round(val[1], 3) * val[0].get_speed_fill_ZU(), 3)
                            val[0].add_ZU(data_filling, t, self.getName())
                            log_cur += "%s СФОТОГРАФИРОВАЛ %s Gbit (%s s * %s Gbit/s)\n" % (
                                val[0].get_name(),
                                round(round(val[1], 3) * val[0].get_speed_fill_ZU(), 3),
                                round(val[1], 3),
                                val[0].get_speed_fill_ZU()
                            )

                t += (self.offset + 1)

                if log_cur != '':
                    log_cur = ("%s - %s\n" % (t, t + offset)) + log_cur + "\n\n"

                log += log_cur

                if log_count == 100:
                    with open("./logs/stations/" + self.station + "_log.txt", 'a+', encoding='utf-8') as file:
                        file.write(log)
                    log = ''
                    log_count = 0

                log_count += 1

            with open("./logs/stations/" + self.station + "_log.txt", 'a+', encoding='utf-8') as file:
                file.write(log)

            with self.lock:
                #print("[+] Thread %s is ended. Time spent: %s" % (self.getName(), time.time() - time_start))
                info("Thread %s is ended. Time spent: %s" % (self.getName(), round(time.time() - time_start, 2)))


        except Exception as e:
            warn(str(e))
            exit()