from Sattelites import KinoSattelite, ZorkiySattelite
from Station import Station
from Communication_session import Session
from Parser import *
#import functools

from threading import Lock
from Thread_for_intervals import *
from datetime import datetime
import time
import argparse
import textwrap
import os
import pyfiglet

offset = 30

'''Сорировка списка пролета над РФ по времени влета'''
def my_sort_time_rus_begin(time_rus_1, time_rus_2):
    if time_rus_1[0] > time_rus_2[0]:
        return 1
    else:
        return -1

'''Сорировка расписания сброса информации по времени начала сброса'''
def my_sort_shedule_list_station(list1, list2):
    if list1[0] > list2[0]:
        return 1
    else:
        return -1


'''главная функция программы, точка входа'''
if __name__ == "__main__":
    '''время начала и конца составления расписания по умолчанию'''
    time_begin = '01.06.2027/03:00:01'#1811808001.0
    time_end = '13.06.2027/22:28:03'#1812931201.0

    '''каталоги по умолчанию'''
    datas_ZRV = 'DATA_Files/Facility2Constellation'
    datas_RUS = 'DATA_Files/Russia2Constellation2'

    banner = pyfiglet.figlet_format("Sitronics")
    print(banner)
    description = '''
            СЕРВИС ПЛАНИРОВАНИЯ ПЕРЕДАЧИ ДАННЫХ ОТ КОСМИЧЕСКИХ АППАРАТОВ НА ЗЕМНЫЕ СТАНЦИИ
            по умолчанию:
            - begin = 01.06.2027/03:00:01
            - end = 13.06.2027/22:28:03
            - zrv = DATA_Files/Facility2Constellation
            - russia = DATA_Files/Russia2Constellation2
            '''
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawDescriptionHelpFormatter,
        description=textwrap.dedent(description))
    parser.add_argument('-b', '--begin', dest="begin", type=str, help="Начало составления расписания. Например: 01.06.2027/03:00:01")
    parser.add_argument('-e', '--end', dest="end", type=str, help="Конец составления расписания. Например: 01.06.2027/06:26:41")
    parser.add_argument('-z', '--zrv', dest="zrv", type=str, help="Каталог с файлами зон радиовидимости")
    parser.add_argument('-r', '--russia', dest="russia", type=str, help="Каталог с файлами пролета на территорией России")
    args = parser.parse_args()

    str_data_begin = time_begin
    str_data_end = time_end

    if args.begin:
        str_data_begin = args.begin
        time_begin = time.mktime(datetime.strptime(args.begin, "%d.%m.%Y/%H:%M:%S").timetuple())
    else:
        time_begin = time.mktime(datetime.strptime(time_begin, "%d.%m.%Y/%H:%M:%S").timetuple())
    if args.end:
        str_data_end = args.end
        time_end = time.mktime(datetime.strptime(args.end, "%d.%m.%Y/%H:%M:%S").timetuple())
    else:
        time_end = time.mktime(datetime.strptime(time_end, "%d.%m.%Y/%H:%M:%S").timetuple())
    if args.zrv:
        datas_ZRV = args.zrv
    if args.russia:
        datas_RUS = args.russia

    info("Период составления расписания: %s : %s" % (str_data_begin, str_data_end))

    if not os.path.exists(datas_ZRV):
        warn("Каталог не найден: %s" % datas_ZRV)
        exit()
    else:
        info("Каталог существует: %s" % datas_ZRV)
    if not os.path.exists(datas_RUS):
        warn("Каталог не найден: %s" % datas_RUS)
        exit()
    else:
        info("Каталог существует: %s" % datas_RUS)

    dir1 = 'shedules'
    dir2 = 'logs'
    dir3 = os.path.join(dir2, 'sattelites')
    dir4 = os.path.join(dir2, 'stations')
    if not os.path.exists(dir1):
        os.makedirs(dir1)
        info("Создан каталог: %s" % dir1)
    else:
        info("Каталог существует: %s" % dir1)
    if not os.path.exists(dir3):
        os.makedirs(dir3)
        info("Создан каталог: %s" % dir3)
    else:
        info("Каталог существует: %s" % dir3)
    if not os.path.exists(dir4):
        os.makedirs(dir4)
        info("Создан каталог: %s" % dir4)
    else:
        info("Каталог существует: %s" % dir4)

    time_main_start = time.time()

    info("Обработка файлов из каталога: %s" % datas_ZRV)
    Facility2Constellation = Parser(datas_ZRV).parse()
    info("Обработка файлов из каталога: %s" % datas_RUS)
    Russia2Constellation = Parser(datas_RUS).parse()

    sattelite_dict = {}
    station_dict = {}
    sessions_dict = {}
    for i in Facility2Constellation:

        for j in i:
            station_name = j[0]
            sattelite_name = j[1]
            if station_dict.get(station_name) is None:
                station_dict[station_name] = Station(station_name)

            if sattelite_dict.get(sattelite_name) is None:
                satt_number = int(sattelite_name.split('_')[1])
                if 110101 <= satt_number <= 110510:
                    sattelite_dict[sattelite_name] = KinoSattelite(sattelite_name)
                    #sattelite_dict.get(sattelite_name).add_ZU(random.randint(0, 8192))
                elif 110601 <= satt_number <= 112010:
                    sattelite_dict[sattelite_name] = ZorkiySattelite(sattelite_name)
                    #sattelite_dict.get(sattelite_name).add_ZU(random.randint(0, 4096))
                else:
                    exit('Что за НЛО?')
            if sessions_dict.get(station_name) is None:
                sessions_dict[station_name] = []

            session = Session(station=station_dict.get(station_name),
                              sattelite=sattelite_dict.get(sattelite_name),
                              time_begin=j[2], time_end=j[3], len_session=j[4])

            sessions_dict[station_name].append(session)

    for i in Russia2Constellation:
        for j in i:
            sattelite_dict.get(j[1]).add_rus_time([j[2], j[3], j[4]])

    for sattelite, obj_satt in sattelite_dict.items():
        obj_satt.get_rus_time().sort(key=functools.cmp_to_key(my_sort_time_rus_begin))

    lock = Lock()

    threads = []
    for station, sessions_list in sessions_dict.items():
        t = myThreadIntervals(lock=lock, station=station, sessions_list=sessions_list,
                              time_begin=time_begin, time_end=time_end, offset=offset)
        t.daemon = True
        t.name = station
        threads.append(t)

    for t in threads:
        t.start()

    for t in threads:
        t.join()

    '''for station, obj_station in station_dict.items():
        print(obj_station.get_schedule())
        shedule_station_dict_new = {}
        #print(station)
        for satt_name, satt_times in obj_station.get_schedule().items():
            shedule_station_dict_new[satt_name] = []
            satt_times_new = []
            for times_i in range(0, len(satt_times) - 1, 2):
                if len(compare_segments(satt_times[times_i], satt_times[times_i + 1])) == 1:
                    satt_times_new.append(satt_times[times_i][0])
                    satt_times_new.append(satt_times[times_i + 1][1])
                else:
                    if satt_times_new:
                        shedule_station_dict_new[satt_name].append([satt_times_new[0], satt_times_new[-1]])
                        satt_times_new = []
                    shedule_station_dict_new[satt_name].append([satt_times[times_i][0], satt_times[times_i][1]])
            shedule_station_dict_new[satt_name].append([satt_times[-1][0], satt_times[-1][1]])

        obj_station.push_schedule(shedule_station_dict_new)'''

    '''Сохранение расписаний'''
    for station, obj_station in station_dict.items():
        shedule_list_station = []
        for satt_name, satt_times in obj_station.get_schedule().items():
            for times in satt_times:
                shedule_list_station.append([times[0], times[1], satt_name])
        shedule_list_station.sort(key=functools.cmp_to_key(my_sort_shedule_list_station))
        sched_dir = "./shedules/" + station + "_shedule.txt"
        with open(sched_dir, 'w+', encoding='utf-8') as file:
            for i in shedule_list_station:
                file.write("%s - %s\t%s\n" % (datetime.utcfromtimestamp(i[0]).strftime('%Y-%m-%d %H:%M:%S') + "." + str(i[0]).split('.')[1],
                                              datetime.utcfromtimestamp(i[1]).strftime('%Y-%m-%d %H:%M:%S') + "." + str(i[1]).split('.')[1],
                                              i[2]))
        plus("Расписание сохранено: %s" % sched_dir)

    info("Return to Thread Main")
    plus("Программа успешно завершила свою работу")
    info("Time: %s" % round(time.time() - time_main_start, 2))
