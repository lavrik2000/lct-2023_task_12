from colorama import init, Fore

init()
RED = Fore.RED
GREEN = Fore.GREEN
WHITE = Fore.WHITE
BLUE = Fore.BLUE
YELLOW = Fore.YELLOW
RESET = Fore.RESET
MAGENTA = Fore.MAGENTA
CYAN = Fore.CYAN


def plus(text):
    print(GREEN + "[+] %s" % text + RESET)

def warn(text):
    print(RED + "[-] %s" % text + RESET)

def info(text):
    print(YELLOW + "[i] %s" % text + RESET)

def green_text(text):
        return GREEN + text + RESET

def red_text(text):
    return RED + text + RESET

def magenta_text(text):
    return MAGENTA + text + RESET

def blue_text(text):
    return BLUE + text + RESET

def cyan_text(text):
    return CYAN + text + RESET